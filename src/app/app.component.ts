import { Component, OnInit } from '@angular/core';
import { encrypt } from './utils/encrypt';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public date;
  constructor(
  ) {
    this.date =  new Date();
    setInterval(() => {
        this.date =  new Date();
     }, 1000);
  }

  ngOnInit() {
    $('#sidebarCollapse').on('click', function () {
      $('#sidebar, #content, #content .navbar').toggleClass('active');
      $('.collapse.in').toggleClass('in');
      $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
  }
}
