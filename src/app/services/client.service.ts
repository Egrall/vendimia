import { Injectable } from '@angular/core';
import { DataService } from '../common/data.service';
import { Client } from '../models/client.model';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

@Injectable()
export class ClientService {
  public route: string;
  constructor(
    private dataService: DataService
  ) {
    this.route = 'clients';
  }

  create(client: Client) {
    return this.dataService.post(this.route, client);
  }

  list() {
    return this.dataService.get('endpoint/' + this.route + '/list');
  }

  findOne(id) {
    return this.dataService.get(this.route + '/' + id);
  }

  update(id, client: Client) {
    return this.dataService.put(this.route, id, client);
  }

  delete(id) {
    return this.dataService.delete(this.route, id);
  }

  search(terms: Observable<string>) {
    return terms.debounceTime(800).distinctUntilChanged()
    .switchMap(term => this.dataService.get('endpoint/' + this.route + '/search/' + term));
  }

}
