import { Injectable } from '@angular/core';
import { DataService } from '../common/data.service';
import { Article } from '../models/article.model';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

@Injectable()
export class ArticlesService {
  public route: string;
  constructor(
    private dataService: DataService
  ) {
    this.route = 'articles';
  }

  create(article: Article) {
    return this.dataService.post(this.route, article);
  }

  list() {
    return this.dataService.get('endpoint/' + this.route + '/list');
  }

  add(id, stock) {
    return this.dataService.post('endpoint/' + this.route + '/addStock/' + id, {stock: stock});
  }

  remove(id, stock) {
    return this.dataService.post('endpoint/' + this.route + '/substractStock/' + id, {stock: stock});
  }


  findOne(id) {
    return this.dataService.get(this.route + '/' + id);
  }

  update(id, article: Article) {
    return this.dataService.put(this.route, id, article);
  }

  delete(id) {
    return this.dataService.delete(this.route, id);
  }

  search(terms: Observable<string>) {
    return terms.debounceTime(800).distinctUntilChanged()
    .switchMap(term => this.dataService.get('endpoint/' + this.route + '/search/' + term));
  }

}
