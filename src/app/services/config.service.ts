import { Injectable } from '@angular/core';
import { DataService } from '../common/data.service';
import { Config } from '../models/config.model';

@Injectable()
export class ConfigService {
  public route: string;
  constructor(
    private dataService: DataService
  ) {
    this.route = 'configs';
  }

  create(config: Config) {
    return this.dataService.post(this.route, config);
  }

  list() {
    return this.dataService.get(this.route);
  }

  findOne(id) {
    return this.dataService.get(this.route + '/' + id);
  }

  update(id, config: Config) {
    return this.dataService.put(this.route, id, config);
  }

  updateConfigs(configs) {
    return this.dataService.post('endpoint/' + this.route + '/updateConfig', configs);
  }

  delete(id) {
    return this.dataService.delete(this.route, id);
  }

}
