import { Injectable } from '@angular/core';
import { DataService } from '../common/data.service';
import { Sale } from '../models/sale.model';

@Injectable()
export class SalesService {
  public route: string;
  constructor(
    private dataService: DataService
  ) {
    this.route = 'sales';
  }

  create(sale: Sale) {
    return this.dataService.post(this.route, sale);
  }

  list() {
    return this.dataService.get('endpoint/' + this.route + '/list');
  }

  findOne(id) {
    return this.dataService.get(this.route + '/' + id);
  }

  update(id, sale: Sale) {
    return this.dataService.put(this.route, id, sale);
  }

  delete(id) {
    return this.dataService.delete(this.route, id);
  }

  addDetail(item) {
    return this.dataService.post('detailSales', item);
  }

}
