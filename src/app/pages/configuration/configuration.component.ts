import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SweetAlertService } from 'angular-sweetalert-service';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfigService } from '../../services/config.service';
import { Config } from '../../models/config.model';
import { decrypt } from '../../utils/encrypt';
import { forkJoin } from 'rxjs/observable/forkJoin';
@Component({
  selector: 'configuration',
  moduleId: module.id,
  templateUrl: './configuration.component.html'
})

export class ConfigurationComponent implements OnInit {
  public title: string;
  public formGroup: FormGroup;
  public configs;
  constructor(
    private fb: FormBuilder,
    private dialogs: SweetAlertService,
    private router: Router,
    private route: ActivatedRoute,
    private configService: ConfigService,
    private titleService: Title
  ) {
    this.configs = {};
    this.title = 'Configuración';
    this.titleService.setTitle('La Vendimia - ' + this.title);
    this.formGroup = fb.group({
      financingRate: ['', Validators.required],
      deposit: ['', Validators.required],
      deadline: ['', Validators.required]
    });
  }

  submit(event) {
    if (event.type === 'keypress' && event.keyCode !== 13) {
      return;
    } else if (event.type === 'click') {
        event.preventDefault();
    }
    forkJoin([
      this.configService.updateConfigs({key: 'financingRate', value: this.configs.financingRate}),
      this.configService.updateConfigs({key: 'deposit', value: this.configs.deposit}),
      this.configService.updateConfigs({key: 'deadline', value: this.configs.deadline})
    ]).subscribe(
      results => {
        this.dialogs.alert({
          title: '¡Bien Hecho!',
          text: 'La configuración ha sido registrada',
          confirmButtonText: 'Aceptar'
        }).then(() => {
          this.router.navigate(['/sales']);
        });
      },
      error => {
        console.log(error);
      }
    );
  }

  cancel() {
    this.dialogs.confirm({
      title: '¡Confirmación!',
      text: '¿Desea salir de la pantalla actual?',
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar'
    }).then(res => {
      if (res.value === true) {
        this.router.navigate(['/sales']);
      }
    });
  }

  ngOnInit() {
    this.configService.list().subscribe(
      result => {
        if (result.status === 'success') {
          if (result.data.length > 0) {
            result.data.map((item, i, array) => {
              this.configs[item.key] = item.value;
            });
          }
        } else {
          console.log(result);
        }
      },
      error => {
        console.log(error);
      }
    );
  }
}
