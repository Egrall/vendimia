import { NgModule } from '@angular/core';
import { ArticlesRoutes, navigateComponents } from './articles.routing';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ArticlesService } from '../../services/article.service';

@NgModule({
  imports: [
    CommonModule,
    ArticlesRoutes,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [],
  declarations: [
    ...navigateComponents
  ],
  providers: [ArticlesService]
})
export class ArticlesModule { }
