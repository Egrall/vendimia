import { Component, OnInit } from '@angular/core';
import { Article } from '../../../models/article.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ArticlesService } from '../../../services/article.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SweetAlertService } from 'angular-sweetalert-service';

@Component({
  selector: 'add',
  moduleId: module.id,
  templateUrl: './add.component.html'
})

export class AddComponent implements OnInit {
  public article: Article;
  public title: string;
  public formGroup: FormGroup;
  constructor(
    private articleService: ArticlesService,
    private fb: FormBuilder,
    private dialogs: SweetAlertService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.article = new Article();
    this.title = 'Registrar';
    this.formGroup = fb.group({
      description: ['', Validators.required],
      model: ['', Validators.required],
      price: ['', Validators.required],
      stock: ['', Validators.required]
    });
  }

  padLeft(text: string, padChar: string, size: number): string {
    return (String(padChar).repeat(size) + text).substr( (size * -1), size) ;
  }

  submit(event) {
    if (event.type === 'keypress' && event.keyCode !== 13) {
      return;
    } else if (event.type === 'click') {
        event.preventDefault();
    }
    this.articleService.update(this.article.id, this.article).subscribe(
      result => {
        if (result.status === 'success') {
          this.dialogs.alert({
            title: '¡Bien Hecho!',
            text: 'El artículo ha sido registrado correctamente',
            confirmButtonText: 'Aceptar'
          }).then(() => {
            this.router.navigate(['../list'], { relativeTo: this.route});
          });
        } else {
          console.log(result);
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  cancel() {
    this.dialogs.confirm({
      title: '¡Confirmación!',
      text: '¿Desea salir de la pantalla actual?',
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar'
    }).then(res => {
      if (res.value === true) {
        this.router.navigate(['../list'], { relativeTo: this.route});
      }
    });
  }

  ngOnInit() {
    this.articleService.create(this.article).subscribe(
      result => {
        if (result.status === 'success') {
          this.article.id = result.data.id;
        } else {
          console.log(result);
        }
      },
      error => {
        console.log(error);
      }
    );
  }
}
