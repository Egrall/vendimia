import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'articles',
  moduleId: module.id,
  templateUrl: './articles.component.html'
})

export class ArticlesComponent implements OnInit {
  public title: string;
  constructor(
    private titleService: Title
  ) {
    this.title = 'Artículos';
    this.titleService.setTitle('La Vendimia - ' + this.title);
  }

  ngOnInit() { }
}
