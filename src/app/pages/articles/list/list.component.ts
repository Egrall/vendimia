import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ArticlesService } from '../../../services/article.service';
import { encrypt } from '../../../utils/encrypt';
import { Article } from '../../../models/article.model';

@Component({
  selector: 'list',
  moduleId: module.id,
  templateUrl: './list.component.html'
})

export class ListComponent implements OnInit {
  public title: string;
  public articles: Article[] = [];
  constructor(
    private articleService: ArticlesService,
    private router: Router,
    private route: ActivatedRoute
  ) {
  }

  navigate(route: string, param: string) {
    this.router.navigate([route, encrypt(String(param))],  {relativeTo: this.route});
  }

  padLeft(text: string, padChar: string, size: number): string {
    return (String(padChar).repeat(size) + text).substr( (size * -1), size) ;
  }

  ngOnInit() {
    this.articleService.list().subscribe(
      result => {
        if (result.status === 'success') {
          this.articles = result.data;
        } else {
          console.log(result);
        }
      },
      error => {
        console.log(error);
      }
    );
  }
}
