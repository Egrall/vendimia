import { NgModule } from '@angular/core';
import { ClientsRoutes, navigateComponents } from './clients.routing';
import { CommonModule } from '@angular/common';
import { ClientService } from '../../services/client.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ClientsRoutes,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [],
  declarations: [
    ...navigateComponents
  ],
  providers: [ClientService]
})
export class ClientsModule { }
