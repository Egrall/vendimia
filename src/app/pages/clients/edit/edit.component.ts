import { Component, OnInit } from '@angular/core';
import { Client } from '../../../models/client.model';
import { ClientService } from '../../../services/client.service';
import { ActivatedRoute, Router } from '@angular/router';
import { decrypt } from '../../../utils/encrypt';
import { SweetAlertService } from 'angular-sweetalert-service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'edit',
  templateUrl: '../add/add.component.html'
})

export class EditComponent implements OnInit {
  public client: Client;
  public title: string;
  public formGroup: FormGroup;
  constructor(
    private clientService: ClientService,
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private dialogs: SweetAlertService
  ) {
    this.client = new Client();
    this.title = 'Editar';
    this.formGroup = fb.group({
      name: ['', Validators.required],
      lastName: ['', Validators.required],
      momLastName: ['', Validators.required],
      rfc: ['', Validators.required]
    });
  }

  padLeft(text: string, padChar: string, size: number): string {
    return (String(padChar).repeat(size) + text).substr( (size * -1), size) ;
  }

  submit(event) {
    if (event.type === 'keypress' && event.keyCode !== 13) {
      return;
    } else if (event.type === 'click') {
        event.preventDefault();
    }
    this.clientService.update(this.client.id, this.client).subscribe(
      result => {
        if (result.status === 'success') {
          this.dialogs.alert({
            title: '¡Bien Hecho!',
            text: 'El Articulo ha sido modificado correctamente',
            confirmButtonText: 'Aceptar'
          }).then(() => {
            this.router.navigate(['../../list'], { relativeTo: this.route});
          });
        } else {
          console.log(result);
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  cancel() {
    this.dialogs.confirm({
      title: '¡Confirmación!',
      text: '¿Desea salir de la pantalla actual?',
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar'
    }).then(res => {
      if (res.value === true) {
        this.router.navigate(['../../list'], { relativeTo: this.route});
      }
    });
  }

  ngOnInit() {
    this.route.paramMap.subscribe(
      p => {
        this.clientService.findOne(decrypt(p.get('id'))).subscribe(
          result => {
            if (result.status === 'success') {
              this.client = result.data;
            } else {
              console.log(result);
            }
          },
          error => {
            console.log(error);
          }
        );
      }
    );
  }
}
