import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../../services/client.service';
import { Client } from '../../../models/client.model';
import { Router, ActivatedRoute } from '@angular/router';
import { encrypt } from '../../../utils/encrypt';

@Component({
  selector: 'list',
  moduleId: module.id,
  templateUrl: './list.component.html'
})

export class ListComponent implements OnInit {
  public title: string;
  public clients: Client[] = [];
  constructor(
    private clientService: ClientService,
    private router: Router,
    private route: ActivatedRoute
  ) {
  }

  padLeft(text: string, padChar: string, size: number): string {
    return (String(padChar).repeat(size) + text).substr( (size * -1), size) ;
  }
  navigate(route: string, param: string) {
    this.router.navigate([route, encrypt(String(param))],  {relativeTo: this.route});
  }
  ngOnInit() {
    this.clientService.list().subscribe(
      result => {
        if (result.status === 'success') {
          this.clients = result.data;
        } else {
          console.log(result);
        }
      },
      error => {
        console.log(error);
      }
    );
  }
}
