import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'clients',
  moduleId: module.id,
  templateUrl: './clients.component.html'
})

export class ClientsComponent implements OnInit {
  public title: string;
  constructor(
    private titleService: Title
  ) {
    this.title = 'Clientes';
    this.titleService.setTitle('La Vendimia - ' + this.title);
  }

  ngOnInit() { }
}
