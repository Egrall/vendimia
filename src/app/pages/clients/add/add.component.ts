import { Component, OnInit } from '@angular/core';
import { Client } from '../../../models/client.model';
import { ClientService } from '../../../services/client.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SweetAlertService } from 'angular-sweetalert-service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'add',
  moduleId: module.id,
  templateUrl: './add.component.html'
})

export class AddComponent implements OnInit {
  public client: Client;
  public title: string;
  public formGroup: FormGroup;
  constructor(
    private clientService: ClientService,
    private fb: FormBuilder,
    private dialogs: SweetAlertService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.client = new Client();
    this.title = 'Registrar';
    this.formGroup = fb.group({
      name: ['', Validators.required],
      lastName: ['', Validators.required],
      momLastName: ['', Validators.required],
      rfc: ['', Validators.required]
    });
  }

  padLeft(text: string, padChar: string, size: number): string {
    return (String(padChar).repeat(size) + text).substr( (size * -1), size) ;
  }

  submit(event) {
    if (event.type === 'keypress' && event.keyCode !== 13) {
      return;
    } else if (event.type === 'click') {
        event.preventDefault();
    }
    this.clientService.update(this.client.id, this.client).subscribe(
      result => {
        if (result.status === 'success') {
          this.dialogs.alert({
            title: '¡Bien Hecho!',
            text: 'El Articulo ha sido registrado correctamente',
            confirmButtonText: 'Aceptar'
          }).then(() => {
            this.router.navigate(['../list'], { relativeTo: this.route});
          });
        } else {
          console.log(result);
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  cancel() {
    this.dialogs.confirm({
      title: '¡Confirmación!',
      text: '¿Desea salir de la pantalla actual?',
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar'
    }).then(res => {
      if (res.value === true) {
        this.router.navigate(['../list'], { relativeTo: this.route});
      }
    });
  }

  ngOnInit() {
    this.clientService.create(this.client).subscribe(
      result => {
        if (result.status === 'success') {
          this.client.id = result.data.id;
        } else {
          console.log(result);
        }
      },
      error => {
        console.log(error);
      }
    );
  }
}
