import { ListComponent } from './list/list.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { SalesComponent } from './sales.component';
import { AddComponent } from './add/add.component';

export const routes: Routes = [
  {
    path: '',
    component: SalesComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'list' },
      { path: 'list', component: ListComponent },
      { path: 'add', component: AddComponent },
    ]
  }
];

export const navigateComponents = [
  SalesComponent,
  ListComponent,
  AddComponent
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class SalesRoutes {}
