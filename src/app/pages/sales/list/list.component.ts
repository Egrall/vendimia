import { Component, OnInit } from '@angular/core';
import { SalesService } from '../../../services/sale.service';
import { Sale } from '../../../models/sale.model';

@Component({
  selector: 'list',
  moduleId: module.id,
  templateUrl: './list.component.html'
})

export class ListComponent implements OnInit {
  public title: string;
  public sales: Sale[] = [];
  constructor(
    private saleService: SalesService
  ) {
  }

  padLeft(text: string, padChar: string, size: number): string {
    return (String(padChar).repeat(size) + text).substr( (size * -1), size) ;
  }

  ngOnInit() {
    this.saleService.list().subscribe(
      result => {
        if (result.status === 'success') {
          this.sales = result.data;
        } else {
          console.log(result);
        }
      },
      error => {
        console.log(error);
      }
    );
  }
}
