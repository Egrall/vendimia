import { NgModule } from '@angular/core';
import { SalesRoutes, navigateComponents } from './sales.routing';
import { CommonModule } from '@angular/common';
import { SalesService } from '../../services/sale.service';
import { ClientService } from '../../services/client.service';
import { ArticlesService } from '../../services/article.service';
import { ConfigService } from '../../services/config.service';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    SalesRoutes,
    FormsModule
  ],
  exports: [],
  declarations: [
    ...navigateComponents
  ],
  providers: [SalesService, ClientService, ArticlesService, ConfigService]
})
export class SalesModule { }
