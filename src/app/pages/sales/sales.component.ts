import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'sales',
  moduleId: module.id,
  templateUrl: './sales.component.html'
})

export class SalesComponent implements OnInit {
  public title: string;
  public sales = [];
  constructor(
    private titleService: Title
  ) {
    this.title = 'Ventas';
    this.titleService.setTitle('La Vendimia - ' + this.title);
    this.sales.push({id: 1, client: 1, name: 'Jose Solano', total: 350, date: new Date(2018, 3, 4), status: 'Pendiente'});
    this.sales.push({id: 2, client: 2, name: 'Ramon Rosado', total: 670, date: new Date(2018, 2, 4), status: 'Pendiente'});
  }

  ngOnInit() { }
}
