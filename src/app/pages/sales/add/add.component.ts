import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Sale } from '../../../models/sale.model';
import { SalesService } from '../../../services/sale.service';
import { Subject } from 'rxjs/Subject';
import { ClientService } from '../../../services/client.service';
import { Client } from '../../../models/client.model';
import { Article } from '../../../models/article.model';
import { ArticlesService } from '../../../services/article.service';
import { DetailSale } from '../../../models/detailSale.model';
import { ConfigService } from '../../../services/config.service';
import { Config } from '../../../models/config.model';
import { SweetAlertService } from 'angular-sweetalert-service';
import { Router, ActivatedRoute } from '@angular/router';
import { forkJoin } from 'rxjs/observable/forkJoin';

@Component({
  selector: 'add',
  moduleId: module.id,
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})

export class AddComponent implements OnInit {
  public sale: Sale;
  public plans = [];
  public clients: Client[] = [];
  public client: Client = null;
  public isQuantityZero = false;
  public isNewArticle = true;
  public configs;
  public prevTotal: number;
  public articlesSearch: Article[] = [];
  public articles: DetailSale[] = [];
  public article: Article;
  public clientTerm = new Subject<string>();
  public articleTerm = new Subject<string>();
  public next = false;
  @ViewChild('searchClientInput') searchClientInput: ElementRef;
  @ViewChild('searchArticleInput') searchArticleInput: ElementRef;
  constructor(
    private saleService: SalesService,
    private clientService: ClientService,
    private articleService: ArticlesService,
    private configService: ConfigService,
    private dialogs: SweetAlertService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.configs = {};
    this.sale = new Sale();
  }

  padLeft(text: string, padChar: string, size: number): string {
    return (String(padChar).repeat(size) + text).substr( (size * -1), size) ;
  }

  onRadioChange(item) {
    this.sale.months = item.months;
    console.log(this.sale);
  }

  abonos() {
    this.plan(3);
    this.plan(6);
    this.plan(9);
    this.plan(12);

    this.next = true;
  }

  plan(months, check?) {
    const preCon = this.sale.total / (1 + ((this.sale.financingRate * this.sale.deadline) / 100));
    const total = preCon * (1 + (this.sale.financingRate * months) / 100);
    const payment = total / months;
    const save = this.sale.total - total;
    this.plans.push({months: months, payment: payment, total: total, save: save});
  }


  searchClient(term) {
    if (term !== '') {
      this.clientService.search(this.clientTerm).subscribe(
        result => {
          if (result.status === 'success') {
            if (result.data) {
              this.clients = result.data;
            } else {
              console.log(result);
            }
          } else {
            console.log(<any>result);
          }
        },
        error => {
          console.log(<any>error);
        }
      );
      this.clientTerm.next(term);
    } else {
      this.clients = [];
    }
  }
  searchArticle(term) {
    if (term !== '') {
      this.articleService.search(this.articleTerm).subscribe(
        result => {
          if (result.status === 'success') {
            if (result.data) {
              this.articlesSearch = result.data;
            } else {
              console.log(result);
            }
          } else {
            console.log(<any>result);
          }
        },
        error => {
          console.log(<any>error);
        }
      );
      this.articleTerm.next(term);
    } else {
      this.articlesSearch = [];
    }
  }

  onArticleSelected(item: Article) {
    this.articlesSearch = [];
    this.searchArticleInput.nativeElement.value = '';
    const article = new DetailSale();
    article.article = item.id;
    article.articleDescription = item.description;
    article.articleModel = item.model;
    article.sale = this.sale.id;
    article.Quantity = 1;
    article.Price = item.price * (1 + (this.sale.financingRate * this.sale.deadline) / 100);
    this.sale.Amount += article.amount;
    this.article = item;
    this.isNewArticle = true;
    this.articles.push(article);
  }

  deleteArticle(item) {
    this.articleService.add(item.article, item.Quantity).subscribe(
      result => {
        if (result.status === 'success') {
          const i = this.articles.indexOf(item);
          this.articles.splice(i, 1);
        } else {
          console.log(result);
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  addArticle() {
    if (this.article) {
      this.articleService.remove(this.article.id, this.articles[this.articles.length - 1].Quantity).subscribe(
        result => {
          if (result.status === 'success') {
            this.isNewArticle = false;
            this.article = null;
          } else {
            if (result.message === 'negative') {
              this.dialogs.alert({
                title: '¡Alerta!',
                text: 'No hay suficientes existencias',
                confirmButtonText: 'Aceptar'
              });
            }
          }
        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.dialogs.alert({
        title: '¡Alerta!',
        text: 'Selecciona un artículo',
        confirmButtonText: 'Aceptar'
      });
    }
  }

  quantityChanged(event, article) {
    if (this.articles.find(x => x.Quantity === 0) !== undefined) {
      this.isQuantityZero = true;
    } else {
      this.isQuantityZero = false;
    }
    this.sale.Amount = 0;
    this.articles.map((item, i, array) => {
      this.sale.Amount += item.amount;
    });
  }
  cancel() {
    this.dialogs.confirm({
      title: '¡Confirmación!',
      text: '¿Desea salir de la pantalla actual?',
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar'
    }).then(res => {
      if (res.value === true) {
        this.router.navigate(['../list'], { relativeTo: this.route});
      }
    });
  }
  readOnlyClicked() {
    this.client = null;

  }
  clientString() {
    return this.padLeft(String(this.client.id), '0', 3)
    + ' - ' + this.client.name + ' ' + this.client.lastName + ' ' + this.client.momLastName;
  }
  onClientSelected(item) {
    this.client = item;
    this.clients = [];
    this.searchClientInput.nativeElement.focus();
  }

  submit() {
    this.sale.client = this.client.id;
    this.sale.clientName = this.client.name + ' ' + this.client.lastName + ' ' + this.client.momLastName;
    this.saleService.update(this.sale.id, this.sale).subscribe(
      result => {
        if (result.status === 'success') {
          const observables = [];
          this.articles.map((item, i, array) => {
            observables.push(this.saleService.addDetail(item));
          });
          forkJoin(observables).subscribe(
            results => {
              console.log(results);
              this.dialogs.alert({
                title: '¡Bien Hecho!',
                text: 'Tu venta ha sido registrado correctamente',
                confirmButtonText: 'Aceptar'
              }).then(() => {
                this.router.navigate(['../list'], { relativeTo: this.route});
              });
            },
            error => {
              console.log(error);
            }
          );
        } else {
          console.log(result);
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  ngOnInit() {
    this.configService.list().subscribe(
      result => {
        if (result.status === 'success') {
          if (result.data.length > 0) {
            result.data.map((item, i, array) => {
              this.configs[item.key] = item.value;
            });
            this.sale.depositP = this.configs.deposit;
            this.sale.deadline = this.configs.deadline;
            this.sale.financingRate = this.configs.financingRate;
            this.saleService.create(this.sale).subscribe(
              res => {
                if (res.status === 'success') {
                  this.sale.id = res.data.id;
                } else {
                  console.log(res);
                }
              },
              error => {
                console.log(error);
              }
            );
          }
        } else {
          console.log(result);
        }
      },
      error => {
        console.log(error);
      }
    );
  }
}
