import * as crypto from 'crypto-js';
import { environment } from '../../environments/environment';

export function encrypt(item) {
  const obj = crypto.AES.encrypt(item, environment.secret);
  return obj.toString();
}

export function decrypt(item) {
  const bytes = crypto.AES.decrypt(item, environment.secret);
  return bytes.toString(crypto.enc.Utf8);
}
