export class Sale {
  public id: number;
  public client: number;
  public clientName: string;
  public deposit: number;
  public depositBon: number;
  public total: number;
  public status: string;
  public months: number;
  public createdAt: Date;
  public financingRate: number;
  public depositP: number;
  public deadline: number;
  public amount = 0;
  constructor() {
  }

  public get Deposit(): number {
    return this.deposit;
  }
  public get Total(): number {
    return this.total;
  }
  public get DepositBon(): number {
    return this.depositBon;
  }
  public get Amount(): number {
    return this.amount;
  }
  public set Amount(v: number) {
    this.amount = v;
    this.deposit = (this.depositP / 100) * this.amount;
    this.depositBon = this.deposit * ((this.financingRate * this.deadline) / 100);
    this.total =  this.amount - this.deposit - this.depositBon;
  }


}
