export class Article {
  public id: number;
  public description: string;
  public model: string;
  public price: number;
  public stock: number;
  constructor() {
  }
}
