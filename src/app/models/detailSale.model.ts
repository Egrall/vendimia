export class DetailSale {
  public id: number;
  public sale: number;
  public article: number;
  public articleDescription: string;
  public articleModel: string;
  private quantity: number;
  private price: number;
  public amount: number;
  constructor() {
  }

  public set Quantity(v: number) {
    this.quantity = Number(v);
    this.amount = this.quantity * this.price;
  }
  public get Quantity(): number {
    return this.quantity;
  }
  public get Price(): number {
    return this.price;
  }
  public set Price(v: number) {
    this.price = v;
    this.amount = this.quantity * this.price;
  }

}
