import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { ConfigurationComponent } from './pages/configuration/configuration.component';
import { SalesModule } from './pages/sales/sales.module';
import { ClientsModule } from './pages/clients/clients.module';
import { ArticlesModule } from './pages/articles/articles.module';
import { NgModule } from '@angular/core';

export const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'sales' },
  { path: 'sales', loadChildren: getSalesRoutes },
  { path: 'clients', loadChildren: getClientsRoutes },
  { path: 'articles', loadChildren: getArticlesRoutes },
  { path: 'config', component: ConfigurationComponent },
];

export function getSalesRoutes() {
  return SalesModule;
}
export function getClientsRoutes() {
  return ClientsModule;
}
export function getArticlesRoutes() {
  return ArticlesModule;
}

export const navigatableComponents = [
  ConfigurationComponent
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule],
  providers: []
})

export class AppRoutes {}
