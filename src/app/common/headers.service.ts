import { Injectable } from '@angular/core';
import {  } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class HeadersService {

  constructor() { }

  get JsonHeaders (): Object {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return { headers: headers };
  }


}
