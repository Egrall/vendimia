import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HeadersService } from './headers.service';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DataService {
  constructor(
    private httpClient: HttpClient,
    private headerService: HeadersService
  ) { }

  public post (route: string, data: any): Observable<any> {
    return this.httpClient
    .post<any>(`${environment.api.host}:${environment.api.port}/${route}`,
        JSON.stringify(data),
        this.headerService.JsonHeaders);
  }

  public get (route: string): Observable<any> {
      return this.httpClient
      .get<any>(`${environment.api.host}:${environment.api.port}/${route}`,
          this.headerService.JsonHeaders);
  }

  public delete (route: string, id: string): Observable<any> {
      return this.httpClient
      .delete<any>(`${environment.api.host}:${environment.api.port}/${route}/${id}`,
          this.headerService.JsonHeaders);
  }

  public put (route: string, id: string, data: any): Observable<any> {
      return this.httpClient
      .put<any>(`${environment.api.host}:${environment.api.port}/${route}/${id}`,
          JSON.stringify(data),
          this.headerService.JsonHeaders);
  }
}
