import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { routes, navigatableComponents, AppRoutes } from './app.routing';
import { DataService } from './common/data.service';
import { HeadersService } from './common/headers.service';
import { HttpClientModule } from '@angular/common/http';
import { SweetAlertService } from 'angular-sweetalert-service';
import { ConfigService } from './services/config.service';


@NgModule({
  declarations: [
    AppComponent,
    ...navigatableComponents
  ],
  imports: [
    BrowserModule,
    AppRoutes,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [DataService, HeadersService, SweetAlertService, ConfigService],
  bootstrap: [AppComponent]
})
export class AppModule { }
